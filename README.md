# Pseudomonas type strain project

* Improve genomic coverage of Pseudomonas diversity by sequencing of type strains

## Pseudomonas phylogeny

* Trees to be added here
   * 100 panorthologs tree for type strains
   * 100 panorthologs tree for all sequenced Pseudomonas genomes

## Phylogenetic markers

* Clustering of gene families with Orthofinder[https://github.com/davidemms/OrthoFinder] (Emms and Kelly, 2015)
* From a set of 668 single copy panorthologs 100 proteins were selected based on Robinson-Foulds distances of single gene trees compared to a 668 concatenated panorthologs tree

| Panortholog          | Pseudomonas aeruginosa PAO1 Locus Tag | Product Name                                             |
|----------------------|---------------------------------------|----------------------------------------------------------|
| OG0001034            | PA3297                                | probable ATP-dependent helicase                          |
| OG0001036            | PA3308                                | RNA helicase HepA                                        |
| OG0001016            | PA3257                                | AlgO                                                     |
| OG0001023            | PA1005                                | conserved hypothetical protein                           |
| OG0001020            | PA1294                                | ribonuclease D                                           |
| OG0001007            | PA2981                                | tetraacyldisaccharide 4-kinase                           |
| OG0001000            | PA2961                                | DNA polymerase III - delta prime subunit                 |
| OG0001055            | PA0759                                | conserved hypothetical protein                           |
| OG0001001            | PA2964                                | 4-amino-4-deoxychorismate lyase                          |
| OG0001012            | PA3243                                | cell division inhibitor MinC                             |
| OG0001167            | PA4044                                | 1-deoxyxylulose-5-phosphate synthase                     |
| OG0001110            | PA4423                                | conserved hypothetical protein                           |
| OG0001095            | PA4393                                | AmpG                                                     |
| OG0001153            | PA4763                                | DNA repair protein RecN                                  |
| OG0001142            | PA4749                                | phosphoglucosamine mutase                                |
| OG0001092            | PA4233                                | probable major facilitator superfamily (MFS) transporter |
| OG0001173            | PA3949                                | hypothetical protein                                     |
| OG0001119            | PA4446                                | AlgW protein                                             |
| OG0001183            | PA4000                                | RlpA                                                     |
| OG0001166            | PA4051                                | thiamine monophosphate kinase                            |
| OG0001232            | PA5045                                | penicillin-binding protein 1A                            |
| OG0001251            | PA5242                                | polyphosphate kinase                                     |
| OG0001250            | PA5241                                | exopolyphosphatase                                       |
| OG0001245            | PA5224                                | aminopeptidase P                                         |
| OG0001244            | PA5221                                | probable FAD-dependent monooxygenase                     |
| OG0001212            | PA0411                                | twitching motility protein PilJ                          |
| OG0001184            | PA4001                                | soluble lytic transglycosylase B                         |
| OG0001259            | PA0308                                | hypothetical protein                                     |
| OG0001208            | PA4969                                | Cyclic AMP (cAMP) Phosphodiesterase - CpdA               |
| OG0001239            | PA5064                                | hypothetical protein                                     |
| OG0001299            | PA4725                                | two-component sensor CbrA                                |
| OG0001283            | PA4542                                | ClpB protein                                             |
| OG0001267            | PA5146                                | hypothetical protein                                     |
| OG0001285            | PA5345                                | ATP-dependent DNA helicase RecG                          |
| OG0001316            | PA3831                                | leucine aminopeptidase                                   |
| OG0001298            | PA4727                                | poly(A) polymerase                                       |
| OG0001273            | PA5361                                | two-component sensor PhoR                                |
| OG0001262            | PA5134                                | carboxyl-terminal processing protease - CtpA             |
| OG0001309            | PA5257                                | hypothetical protein                                     |
| OG0001261            | PA5133                                | conserved hypothetical protein                           |
| OG0001350            | PA3658                                | protein-PII uridylyltransferase                          |
| OG0001369            | PA3620                                | DNA mismatch repair protein MutS                         |
| OG0001382            | PA3456                                | hypothetical protein                                     |
| OG0001392            | PA0008                                | glycyl-tRNA synthetase beta chain                        |
| OG0001362            | PA3638                                | conserved hypothetical protein                           |
| OG0001367            | PA3626                                | conserved hypothetical protein                           |
| OG0001333            | PA3800                                | conserved hypothetical protein                           |
| OG0001394            | PA0005                                | lysophosphatidic acid acyltransferase - LptA             |
| OG0001329            | PA3805                                | type 4 fimbrial biogenesis protein PilF                  |
| OG0001388            | PA0944                                | phosphoribosylaminoimidazole synthetase                  |
| OG0001439            | PA3002                                | transcription-repair coupling protein Mfd                |
| OG0001462            | PA5493                                | DNA polymerase I                                         |
| OG0001399            | PA5568                                | conserved hypothetical protein                           |
| OG0001459            | PA5203                                | glutamate--cysteine ligase                               |
| OG0001454            | PA1758                                | para-aminobenzoate synthase component I                  |
| OG0001400            | PA5567                                | conserved hypothetical protein                           |
| OG0001436            | PA1375                                | erythronate-4-phosphate dehydrogenase                    |
| OG0001493            | PA1614                                | glycerol-3-phosphate dehydrogenase - biosynthetic        |
| OG0001480            | PA0375                                | cell division protein FtsX                               |
| OG0001478            | PA0373                                | signal recognition particle receptor FtsY                |
| OG0001584            | PA2858                                | conserved hypothetical protein                           |
| OG0001567            | PA0934                                | GTP pyrophosphokinase                                    |
| OG0001566            | PA3344                                | ATP-dependent DNA helicase RecQ                          |
| OG0001504            | PA3020                                | probable soluble lytic transglycosylase                  |
| OG0001590            | PA5206                                | acetylornithine deacetylase                              |
| OG0001511            | PA3111                                | folylpolyglutamate synthetase                            |
| OG0001508            | PA3087                                | hypothetical protein                                     |
| OG0001572            | PA3200                                | conserved hypothetical protein                           |
| OG0001514            | PA2974                                | probable hydrolase                                       |
| OG0001574            | PA3198                                | conserved hypothetical protein                           |
| OG0001612            | PA3068                                | NAD-dependent glutamate dehydrogenase                    |
| OG0001597            | PA4937                                | exoribonuclease RNase R                                  |
| OG0001677            | PA0586                                | conserved hypothetical protein                           |
| OG0001674            | PA1011                                | hypothetical protein                                     |
| OG0001599            | PA4636                                | hypothetical protein                                     |
| OG0001654            | PA5215                                | glycine-cleavage system protein T1                       |
| OG0001657            | PA2963                                | conserved hypothetical protein                           |
| OG0001638            | PA5258                                | hypothetical protein                                     |
| OG0001683            | PA4397                                | ketopantoate reductase                                   |
| OG0001664            | PA2633                                | hypothetical protein                                     |
| OG0001746            | PA2543                                | conserved hypothetical protein                           |
| OG0001739            | PA3047                                | probable D-alanyl-D-alanine carboxypeptidase             |
| OG0001724            | PA3238                                | hypothetical protein                                     |
| OG0001690            | PA4472                                | PmbA protein                                             |
| OG0001740            | PA5209                                | hypothetical protein                                     |
| OG0001707            | PA5223                                | ubiH protein                                             |
| OG0001695            | PA4617                                | conserved hypothetical protein                           |
| OG0001717            | PA4627                                | conserved hypothetical protein                           |
| OG0001708            | PA5156                                | hypothetical protein                                     |
| OG0001743            | PA5485                                | AmpDh2                                                   |
| OG0000977            | PA3011                                | DNA topoisomerase I                                      |
| OG0000987            | PA1803                                | Lon protease                                             |
| OG0000966            | PA2615                                | cell division protein FtsK                               |
| OG0000986            | PA1805                                | peptidyl-prolyl cis-trans isomerase D                    |
| OG0000994            | PA3075                                | hypothetical protein                                     |
| OG0000993            | PA3074                                | hypothetical protein                                     |
| OG0000975            | PA2630                                | conserved hypothetical protein                           |
| OG0000992            | PA3073                                | hypothetical protein                                     |
| OG0000985            | PA1814                                | KerV                                                     |
| OG0000983            | PA1528                                | cell division protein ZipA                               |


## Contributors
For questions please contact:   
Frederik Schulz <fschulz@lbl.gov>  
Cedar Hesse <Cedar.Hesse@ars.usda.gov>  
Joyce Loper <loperj@science.oregonstate.edu>  